resource "cloudflare_access_identity_provider" "pin_login" {
 count   = var.pin_login ? 1 : 0
 zone_id = var.zone_id
 name    = "${var.environment}: PIN login"
 type    = "onetimepin"
}

resource "cloudflare_access_identity_provider" "keycloak_saml" {
 count   = var.keycloak_saml ? 1 : 0
 zone_id = var.zone_id
 name    = "SAML - Keycloak"
 type    = "saml"
 config {
   issuer_url      = var.issuer_url
   sso_target_url  = var.sso_target_url
   attributes      = ["email"]
   sign_request    = false
   idp_public_cert = var.idp_public_cert
 }
}
