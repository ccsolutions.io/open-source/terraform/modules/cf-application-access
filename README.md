# cf-application-access

## Important note

This repository is used as a module by at least one client project. The client project uses a specific tag of the repository, hence the tags should not be deleted. The repository itself should also not be deleted unless it is clear that no other project is depending on it.

## Overview

CloudFlare Access protects Self-Hosted, SaaS and Private applications with Zero Trust policies.

The example in this repository sets up a policy for a self-hosted application. It's possible to specify different identity providers. A simple one is the pin_login method, which specifies that  access to the application shall be possible only for users on an allow list. These users can access the application via usage of a one-time PIN, which they get sent to their email addresses. There are other identity providers, currently in this repository the only other method that has been implemented is a saml provider which shall be used with Keycloak.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5.3 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 4.13.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_access_application.cf_apps](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_application) | resource |
| [cloudflare_access_group.domains](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_group) | resource |
| [cloudflare_access_group.emails](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_group) | resource |
| [cloudflare_access_identity_provider.keycloak_saml](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_identity_provider) | resource |
| [cloudflare_access_identity_provider.pin_login](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_identity_provider) | resource |
| [cloudflare_access_policy.cf_policy](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/access_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_access_domains"></a> [access\_domains](#input\_access\_domains) | The domain to grant access to the applications | `list(string)` | n/a | yes |
| <a name="input_access_emails"></a> [access\_emails](#input\_access\_emails) | The values of the application to be created | `list(string)` | n/a | yes |
| <a name="input_applications"></a> [applications](#input\_applications) | The values of the application to be created | `map(any)` | n/a | yes |
| <a name="input_cloudflare_email"></a> [cloudflare\_email](#input\_cloudflare\_email) | The login email for CloudFlare | `string` | n/a | yes |
| <a name="input_cloudflare_global_api_key"></a> [cloudflare\_global\_api\_key](#input\_cloudflare\_global\_api\_key) | The global API key for CloudFlare. Further reading: https://developers.cloudflare.com/fundamentals/api/get-started/create-token/ | `string` | n/a | yes |
| <a name="input_domain"></a> [domain](#input\_domain) | The domain name | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | The name of the environment | `string` | n/a | yes |
| <a name="input_idp_public_cert"></a> [idp\_public\_cert](#input\_idp\_public\_cert) | Only for SAML Identity Provider | `any` | n/a | yes |
| <a name="input_issuer_url"></a> [issuer\_url](#input\_issuer\_url) | Only for SAML Identity Provider | `any` | n/a | yes |
| <a name="input_keycloak_saml"></a> [keycloak\_saml](#input\_keycloak\_saml) | Set this to true to enable keycloak\_saml as (additional) identity provider. Disable the method by setting it to false. | `bool` | `false` | no |
| <a name="input_pin_login"></a> [pin\_login](#input\_pin\_login) | Set this to true to enable pin\_login as (additional) identity provider. Disable the method by setting it to false. | `bool` | `true` | no |
| <a name="input_sso_target_url"></a> [sso\_target\_url](#input\_sso\_target\_url) | Only for SAML Identity Provider | `any` | n/a | yes |
| <a name="input_zone_id"></a> [zone\_id](#input\_zone\_id) | The CloudFlare id for the zone | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
