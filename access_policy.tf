resource "cloudflare_access_policy" "cf_policy" {
  for_each       = cloudflare_access_application.cf_apps
  application_id = each.value.id
  zone_id        = var.zone_id
  name           = "tf_policy"
  precedence     = "1"
  decision       = "allow"

  include {
    group = [cloudflare_access_group.keycloak_users.id, cloudflare_access_group.allowlist.id]
  }
  exclude {
    geo = ["CN", "RU"]
  }
  depends_on = [cloudflare_access_application.cf_apps, cloudflare_access_group.keycloak_users, cloudflare_access_group.allowlist]
}

