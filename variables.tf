variable "environment" {
  description = "The name of the environment"
  type        = string
}

variable "cloudflare_email" {
  description = "The login email for CloudFlare"
  type        = string
}

variable "cloudflare_global_api_key" {
  description = "The global API key for CloudFlare. Further reading: https://developers.cloudflare.com/fundamentals/api/get-started/create-token/"
  type        = string
}

variable "zone_id" {
  description = "The CloudFlare id for the zone"
  type        = string
}

variable "domain" {
  description = "The domain name"
  type        = string
}

variable "applications" {
  description = "The values of the application to be created"
  type        = map(any)
}

# variable "access_domains" {
#   description = "The domain to grant access to the applications"
#   type        = list(string)
# }

# variable "access_emails" {
#   description = "The values of the application to be created"
#   type        = list(string)
# }

variable "ip_allowlist" {
  description = "A list of IP addresses that should have access"
  type        = list(string)
}

variable "pin_login" {
  description = "Set this to true to enable pin_login as (additional) identity provider. Disable the method by setting it to false."
  default     = false
}

variable "keycloak_saml" {
  description = "Set this to true to enable keycloak_saml as (additional) identity provider. Disable the method by setting it to false."
  default     = true
}

# Only for SAML Identity Provider
variable "issuer_url" {
  description = ""
}

# Only for SAML Identity Provider
variable "sso_target_url" {
  description = ""
}

# Only for SAML Identity Provider
variable "idp_public_cert" {
  description = ""
}
