# resource "cloudflare_access_group" "domains" {
#   zone_id = var.zone_id
#   name    = "${var.environment}: List of Domains with access"
#   include {
#     email_domain = toset(var.access_domains)
#   }
# }

# resource "cloudflare_access_group" "emails" {
#   zone_id = var.zone_id
#   name    = "${var.environment}: List of emails with access"
#   include {
#     email = toset(var.access_emails)
#   }
# }

resource "cloudflare_access_group" "keycloak_users" {
  zone_id = var.zone_id
  name    = "${var.environment}: Keycloak Users"
  include {
    login_method = ["SAML - Keycloak"]
  }
}

resource "cloudflare_access_group" "allowlist" {
  zone_id = var.zone_id
  name    = "${var.environment}: AllowlistIPs"
  include {
    ip  = toset(var.ip_allowlist)
  }
}
