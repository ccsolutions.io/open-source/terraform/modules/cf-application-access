resource "cloudflare_access_application" "cf_apps" {
  for_each         = var.applications
  zone_id          = var.zone_id
  name             = each.value.name
  domain           = "${each.value.domain}/${each.value.path}"
  type             = "self_hosted"
  session_duration = each.value.session_duration
}
